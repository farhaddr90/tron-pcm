# Tron Full Node Deployment

This guide will walk you through the process of deploying a Tron full node. The full node contains complete historical data and serves as the entry point into the TRON network. It provides both HTTP API and gRPC API for external queries, allowing you to interact with the TRON network for various operations such as transferring assets, deploying contracts, and interacting with contracts.

## Starting the Full Node

To start the mainnet full node, use the following command:

```shell
nohup java -Xmx24g -XX:+UseConcMarkSweepGC -jar FullNode.jar -c main_net_config.conf &
```

In this command, the configuration file of the full node is specified by the `-c` parameter. Adjust the file path accordingly to match your setup.

### Enabling the Event Subscription Service

By default, the event subscription service is disabled. To enable it, add the `--es` command line parameter when starting the node. Use the following command to start the node with the event subscription service enabled:

```shell
nohup java -Xmx24g -XX:+UseConcMarkSweepGC -jar FullNode.jar -c main_net_config.conf --es &
```

## Preparing Event Subscription Script

To subscribe to events, we will use Node.js as an example. Follow these steps to install the required `zeromq` library:

1. Open your terminal.
2. Run the following command to install `zeromq` library using npm:

```shell
$ npm install zeromq@5
```

After installing the `zeromq` library, you can proceed with your event subscription script.

---

Make sure you have the necessary system requirements and dependencies installed before deploying the Tron full node. For more detailed instructions and additional configuration options, please refer to the official Tron documentation.

Happy Tron network interaction!

# tron-simorgh

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:

```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:

```shell script
./mvnw package
```

It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:

```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using:

```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using:

```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/tron-simorgh-1.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Provided Code

### RESTEasy Reactive

Easily start your Reactive RESTful Web Services

[Related guide section...](https://quarkus.io/guides/getting-started-reactive#reactive-jax-rs-resources)
