package com.pcm.api;

import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.crypto.HDKeyDerivation;
import org.bitcoinj.wallet.DeterministicSeed;
import org.tron.trident.core.key.KeyPair;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

@Path("/api")
public class KeyPairResource {

    @POST
    @Path("/rootKey")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getRootKey(String phrase){
        String[] splitWord = phrase.split(" ");
        List<String> mnemonicWords = Arrays.asList(splitWord);
        DeterministicSeed seed = new DeterministicSeed(mnemonicWords, null, "", System.currentTimeMillis());
        // Generate the master key from the seed
        DeterministicKey masterKey = HDKeyDerivation.createMasterPrivateKey(seed.getSeedBytes());
        KeyPair masterKeyPair = new KeyPair(masterKey.getPrivateKeyAsHex());
        return KeyPair.publicKeyToBase58CheckAddress(masterKeyPair.getRawPair().getPublicKey());
    }


}
