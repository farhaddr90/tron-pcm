package com.pcm.model;

import java.util.List;

public class Transaction {
    /**
    "timeStamp": 1529980017000,
            "triggerName": "transactionTrigger",
            "transactionId": "313228c30c01b6abbcab1ee75d0a9510fbc9e94605b756ff7e8befb29d1ded3d",
            "blockHash": "000000000000732d500992694116c4c90a8b7fd683ddc7b7a2b8acb01f43ba82",
            "blockNumber": 29485,
            "energyUsage": 0,
            "energyFee": 0,
            "originEnergyUsage": 0,
            "energyUsageTotal": 0,
            "netUsage": 190,
            "netFee": 0,
            "result": null,
            "contractAddress": null,
            "contractType": "FreezeBalanceContract",
            "feeLimit": 0,
            "contractCallValue": 0,
            "contractResult": null,
            "fromAddress": null,
            "toAddress": null,
            "assetName": null,
            "assetAmount": 0,
            "latestSolidifiedBlockNumber": 29467,
            "internalTransactionList": [],
            "data": "",
            "transactionIndex": 0,
            "cumulativeEnergyUsed": 0,
            "preCumulativeLogCount": 0,
            "logList": null,
            "energyUnitPrice": 0,
    */

    Long timeStamp						;//": 1529980017000,
    String triggerName					;//": "transactionTrigger",
    String transactionId				;//": "313228c30c01b6abbcab1ee75d0a9510fbc9e94605b756ff7e8befb29d1ded3d",
    String blockHash					;//": "000000000000732d500992694116c4c90a8b7fd683ddc7b7a2b8acb01f43ba82",
    Long blockNumber					;//": 29485,
    Integer energyUsage					;//": 0,
    Integer energyFee					;//": 0,
    Integer originEnergyUsage			;//": 0,
    Integer energyUsageTotal			;//": 0,
    Integer netUsage					;//": 190,
    Integer netFee						;//": 0,
    String result						;//": null,
    String contractAddress				;//": null,
    String contractType					;//": "FreezeBalanceContract",
    Integer feeLimit					;//": 0,
    Integer contractCallValue			;//": 0,
    String contractResult				;//": null,
    String fromAddress					;//": null,
    String toAddress					;//": null,
    String assetName					;//": null,
    Long assetAmount					;//": 0,
    Long latestSolidifiedBlockNumber	;//": 29467,
    List<String> internalTransactionList	;//": [],
    String data							;//": "",
    Integer transactionIndex			;//": 0,
    Integer cumulativeEnergyUsed		;//": 0,
    Integer preCumulativeLogCount		;//": 0,
    String logList						;//": null,
    Integer energyUnitPrice				;//": 0,

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

    public Long getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(Long blockNumber) {
        this.blockNumber = blockNumber;
    }

    public Integer getEnergyUsage() {
        return energyUsage;
    }

    public void setEnergyUsage(Integer energyUsage) {
        this.energyUsage = energyUsage;
    }

    public Integer getEnergyFee() {
        return energyFee;
    }

    public void setEnergyFee(Integer energyFee) {
        this.energyFee = energyFee;
    }

    public Integer getOriginEnergyUsage() {
        return originEnergyUsage;
    }

    public void setOriginEnergyUsage(Integer originEnergyUsage) {
        this.originEnergyUsage = originEnergyUsage;
    }

    public Integer getEnergyUsageTotal() {
        return energyUsageTotal;
    }

    public void setEnergyUsageTotal(Integer energyUsageTotal) {
        this.energyUsageTotal = energyUsageTotal;
    }

    public Integer getNetUsage() {
        return netUsage;
    }

    public void setNetUsage(Integer netUsage) {
        this.netUsage = netUsage;
    }

    public Integer getNetFee() {
        return netFee;
    }

    public void setNetFee(Integer netFee) {
        this.netFee = netFee;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getContractAddress() {
        return contractAddress;
    }

    public void setContractAddress(String contractAddress) {
        this.contractAddress = contractAddress;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public Integer getFeeLimit() {
        return feeLimit;
    }

    public void setFeeLimit(Integer feeLimit) {
        this.feeLimit = feeLimit;
    }

    public Integer getContractCallValue() {
        return contractCallValue;
    }

    public void setContractCallValue(Integer contractCallValue) {
        this.contractCallValue = contractCallValue;
    }

    public String getContractResult() {
        return contractResult;
    }

    public void setContractResult(String contractResult) {
        this.contractResult = contractResult;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public Long getAssetAmount() {
        return assetAmount;
    }

    public void setAssetAmount(Long assetAmount) {
        this.assetAmount = assetAmount;
    }

    public Long getLatestSolidifiedBlockNumber() {
        return latestSolidifiedBlockNumber;
    }

    public void setLatestSolidifiedBlockNumber(Long latestSolidifiedBlockNumber) {
        this.latestSolidifiedBlockNumber = latestSolidifiedBlockNumber;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getTransactionIndex() {
        return transactionIndex;
    }

    public void setTransactionIndex(Integer transactionIndex) {
        this.transactionIndex = transactionIndex;
    }

    public Integer getCumulativeEnergyUsed() {
        return cumulativeEnergyUsed;
    }

    public void setCumulativeEnergyUsed(Integer cumulativeEnergyUsed) {
        this.cumulativeEnergyUsed = cumulativeEnergyUsed;
    }

    public Integer getPreCumulativeLogCount() {
        return preCumulativeLogCount;
    }

    public void setPreCumulativeLogCount(Integer preCumulativeLogCount) {
        this.preCumulativeLogCount = preCumulativeLogCount;
    }

    public String getLogList() {
        return logList;
    }

    public List<String> getInternalTransactionList() {
        return internalTransactionList;
    }

    public void setInternalTransactionList(List<String> internalTransactionList) {
        this.internalTransactionList = internalTransactionList;
    }

    public void setLogList(String logList) {
        this.logList = logList;
    }

    public Integer getEnergyUnitPrice() {
        return energyUnitPrice;
    }

    public void setEnergyUnitPrice(Integer energyUnitPrice) {
        this.energyUnitPrice = energyUnitPrice;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "timeStamp=" + timeStamp +
                ", triggerName='" + triggerName + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", blockHash='" + blockHash + '\'' +
                ", blockNumber=" + blockNumber +
                ", energyUsage=" + energyUsage +
                ", energyFee=" + energyFee +
                ", originEnergyUsage=" + originEnergyUsage +
                ", energyUsageTotal=" + energyUsageTotal +
                ", netUsage=" + netUsage +
                ", netFee=" + netFee +
                ", result='" + result + '\'' +
                ", contractAddress='" + contractAddress + '\'' +
                ", contractType='" + contractType + '\'' +
                ", feeLimit=" + feeLimit +
                ", contractCallValue=" + contractCallValue +
                ", contractResult='" + contractResult + '\'' +
                ", fromAddress='" + fromAddress + '\'' +
                ", toAddress='" + toAddress + '\'' +
                ", assetName='" + assetName + '\'' +
                ", assetAmount=" + assetAmount +
                ", latestSolidifiedBlockNumber=" + latestSolidifiedBlockNumber +
                ", internalTransactionList=" + internalTransactionList +
                ", data='" + data + '\'' +
                ", transactionIndex=" + transactionIndex +
                ", cumulativeEnergyUsed=" + cumulativeEnergyUsed +
                ", preCumulativeLogCount=" + preCumulativeLogCount +
                ", logList='" + logList + '\'' +
                ", energyUnitPrice=" + energyUnitPrice +
                '}';
    }
}
