package com.pcm.service;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TronAddress {
    public static String fromPublicKey(byte[] publicKey) {
        byte[] hash = sha256(publicKey);
        byte[] hash2 = ripemd160(hash);
        byte[] hash3 = new byte[21];
        System.arraycopy(hash2, 0, hash3, 0, 20);
        hash3[0] = (byte) 0x41; // Tron address prefix
        return Base58Check.encode(hash3);
    }

    private static byte[] sha256(byte[] input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(input);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private static byte[] ripemd160(byte[] input) {
        RIPEMD160Digest digest = new RIPEMD160Digest();
        digest.update(input, 0, input.length);
        byte[] output = new byte[digest.getDigestSize()];
        digest.doFinal(output, 0);
        return output;
    }

    private static class Base58Check {
        private static final char[] ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz".toCharArray();
        private static final int[] INDEXES = new int[128];

        static {
            for (int i = 0; i < ALPHABET.length; i++) {
                INDEXES[ALPHABET[i]] = i;
            }
        }

        public static String encode(byte[] input) {
            byte[] checksum = sha256(sha256(input));
            byte[] extended = new byte[input.length + 4];
            System.arraycopy(input, 0, extended, 0, input.length);
            System.arraycopy(checksum, 0, extended, input.length, 4);
            return base58encode(extended);
        }

        private static String base58encode(byte[] input) {
            StringBuilder builder = new StringBuilder();
            int leadingZeros = 0;
            for (int i = 0; i < input.length && input[i] == 0; i++) {
                leadingZeros++;
            }
            byte[] copy = new byte[input.length];
            System.arraycopy(input, 0, copy, 0, input.length);
            int index = leadingZeros;
            while (index < copy.length) {
                int remainder = 0;
                for (int i = index; i < copy.length; i++) {
                    int digit256 = (copy[i] & 0xFF) + remainder * 256;
                    copy[i] = (byte) (digit256 / 58);
                    remainder = digit256 % 58;
                }
                builder.append(ALPHABET[remainder]);
                index += 1;
            }
            for (int i = 0; i < leadingZeros; i++) {
                builder.insert(0, ALPHABET[0]);
            }
            return builder.toString();
        }
    }
}
