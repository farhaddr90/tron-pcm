package com.pcm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pcm.model.Transaction;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class GetEvents {

    public static void main(String[] args) throws IOException {
        // create ZeroMQ context
        ZContext context = new ZContext();

        // create ZeroMQ subscriber socket
        ZMQ.Socket subscriber = context.createSocket(SocketType.SUB);

        subscriber.connect("tcp://127.0.0.1:5555");
        subscriber.subscribe("transaction".getBytes());

        File file = new File("D:/out3.txt");
        PrintWriter printWriter = new PrintWriter(file);
        // listen for messages
        ObjectMapper mapper = new ObjectMapper();

        while (true) {
            String topic = new String(subscriber.recv());
            String message = new String(subscriber.recv());
            printWriter.println(topic);
            printWriter.println(message + "\n");
//            System.out.println(topic +"-----------"+message+"\n");

            Transaction transaction = mapper.readValue(message, Transaction.class);
//            if (message.contains("TLLM21wteSPs4hKjbxgmH1L6poyMjeTbHm") || message.contains("TSnjgPDQfuxx72iaPy82v3T8HrsN4GVJzW") || message.contains("TGX6tRfV4CcUH4hbsujqhcL8omACeGu4kQ"))
            System.out.println(transaction);
        }
    }

}
