package com.pcm.service;

import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.tron.trident.crypto.SECP256K1;

import java.math.BigInteger;
import java.security.*;
import java.util.*;

public class Main2 {

    public static SECP256K1.KeyPair generateHDKeyPair() throws NoSuchAlgorithmException {
        byte[] seed = "farhad".getBytes();
        SecureRandom random = new SecureRandom(seed);

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        keyPairGenerator.initialize(256, random);
        KeyPair rawKeyPair = keyPairGenerator.generateKeyPair();

        BCECPrivateKey privateKey = (BCECPrivateKey)rawKeyPair.getPrivate();
        BCECPublicKey publicKey = (BCECPublicKey)rawKeyPair.getPublic();


        BigInteger privateKeyValue = privateKey.getD();
        byte[] publicKeyBytes = publicKey.getQ().getEncoded(false);
        BigInteger publicKeyValue = new BigInteger(1, Arrays.copyOfRange(publicKeyBytes, 1, publicKeyBytes.length));
        return new SECP256K1.KeyPair(SECP256K1.PrivateKey.create(privateKeyValue), SECP256K1.PublicKey.create(publicKeyValue));
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        SECP256K1.KeyPair keyPair = generateHDKeyPair();
        String pubkey = org.tron.trident.core.key.KeyPair.publicKeyToBase58CheckAddress(keyPair.getPublicKey());
        System.out.println(pubkey);
    }
}
