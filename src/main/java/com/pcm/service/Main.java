package com.pcm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;
import com.pcm.model.Transaction;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.bitcoinj.crypto.*;
import org.bitcoinj.wallet.DeterministicSeed;
import org.tron.trident.api.WalletGrpc;
import org.tron.trident.core.ApiWrapper;
import org.tron.trident.core.exceptions.IllegalException;
import org.tron.trident.core.key.KeyPair;
import org.tron.trident.proto.Contract;
import org.tron.trident.proto.Response;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.io.*;
import java.security.*;
import java.util.Arrays;
import java.util.List;

import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.crypto.HDKeyDerivation;
import org.bitcoinj.crypto.MnemonicCode;

import static org.tron.trident.core.ApiWrapper.parseAddress;
import static org.tron.trident.proto.Response.TransactionReturn.response_code.SUCCESS;


public class Main {


    public static void main2(String[] args) {

    }

    public Response.TransactionExtention transfer(String fromAddress, String toAddress, long amount) throws IllegalException {

        WalletGrpc.WalletBlockingStub blockingStub;
        String grpcEndpointMainNet = "grpc.trongrid.io:50051" , grpcEndpointSahsta = "grpc.shasta.trongrid.io:50051" , grpcEndpointNile = "47.252.19.181:50051";
        ManagedChannel channel = ManagedChannelBuilder.forTarget(grpcEndpointMainNet).usePlaintext().build();
        blockingStub = WalletGrpc.newBlockingStub(channel);

        ByteString rawFrom = parseAddress(fromAddress);
        ByteString rawTo = parseAddress(toAddress);

        Contract.TransferContract req = Contract.TransferContract.newBuilder()
                .setOwnerAddress(rawFrom)
                .setToAddress(rawTo)
                .setAmount(amount)
                .build();
        Response.TransactionExtention txnExt = blockingStub.createTransaction2(req);

        if(SUCCESS != txnExt.getResult().getCode()){
            throw new IllegalException(txnExt.getResult().getMessage().toStringUtf8());
        }

        return txnExt;
    }


    public static void getMnemonic() throws MnemonicException, IOException {

        // Generate a random seed with 256 bits of entropy
        byte[] entropy = new byte[SEED_ENTROPY_BITS / 8];
        new SecureRandom().nextBytes(entropy);

        // Create a mnemonic code from the entropy
        MnemonicCode mnemonicCode = new MnemonicCode();
        List<String> mnemonicWords = mnemonicCode.toMnemonic(entropy);

        // Generate a deterministic seed from the mnemonic words
        DeterministicSeed seed = new DeterministicSeed(mnemonicWords, null, "", System.currentTimeMillis());

        // Generate the master key from the seed
        DeterministicKey masterKey = HDKeyDerivation.createMasterPrivateKey(seed.getSeedBytes());

        // Generate the Tron address from the master key
        DeterministicKey tronKey = HDKeyDerivation.deriveChildKey(masterKey, new ChildNumber(195, true));

        //////////////////////////
        tronKey.getPrivKey();
        KeyPair keyPair = new KeyPair(tronKey.getPrivateKeyAsHex());
        String base58 = KeyPair.publicKeyToBase58CheckAddress(keyPair.getRawPair().getPublicKey());
        System.out.println("base58:  ------- "+base58);
        /////////////////////

        String tronAddress = TronAddress.fromPublicKey(tronKey.getPubKey());

        // Print the results
        System.out.println("Mnemonic words: " + String.join(" ", mnemonicWords));
        System.out.println("Tron address: " + tronAddress);
    }

    public static void getTronAddress() {
//        ApiWrapper wrapper = ApiWrapper.ofShasta("hex private key");
        KeyPair keyPair = KeyPair.generate();
        System.out.println("private: " + keyPair.toPrivateKey());
        System.out.println("public: " + keyPair.toPublicKey());

        //the parent function, returns byte[]
        byte[] address = KeyPair.publicKeyToAddress(keyPair.getRawPair().getPublicKey());

        String base58Address = KeyPair.publicKeyToBase58CheckAddress(keyPair.getRawPair().getPublicKey());

        String hexAddress = KeyPair.publicKeyToHexAddress(keyPair.getRawPair().getPublicKey());

        System.out.println("private: "+ keyPair.toPrivateKey());
        System.out.println("address: " + new String(address));
        System.out.println("base58Address: " + base58Address);
        System.out.println("hexAddress: " + hexAddress);

        ApiWrapper apiWrapper = ApiWrapper.ofShasta(hexAddress);

    }

    private static final int SEED_ENTROPY_BITS = 256;

    public static void main(String[] args) {

        List<String> mnemonicWords = Arrays.asList("spin", "miracle", "lonely", "angry", "egg",
                "again", "knock", "punch", "meadow", "silly", "shove", "ability"
                , "math", "cream", "announce", "begin", "cotton", "ability", "negative",
                "rapid", "cushion", "staff", "expire", "better");

        // Print the results
        System.out.println("Mnemonic words: " + String.join(" ", mnemonicWords));
        DeterministicSeed seed = new DeterministicSeed(mnemonicWords, null, "", System.currentTimeMillis());





        // Generate the master key from the seed
        //-------------------------------------------------------------------------------------------------------------------------
        DeterministicKey masterKey = HDKeyDerivation.createMasterPrivateKey(seed.getSeedBytes());

        KeyPair masterKeyPair = new KeyPair(masterKey.getPrivateKeyAsHex());
        String base58Master = KeyPair.publicKeyToBase58CheckAddress(masterKeyPair.getRawPair().getPublicKey());
        System.out.println("master: "+base58Master);
        //-------------------------------------------------------------------------------------------------------------------------








        // Generate the Tron address from the master key
        //-------------------------------------------------------------------------------------------------------------------------
        DeterministicKey tronKey = HDKeyDerivation.deriveChildKey(masterKey, new ChildNumber(195, true));

        KeyPair keyPair = new KeyPair(tronKey.getPrivateKeyAsHex());
        String base58 = KeyPair.publicKeyToBase58CheckAddress(keyPair.getRawPair().getPublicKey());
        System.out.println("child-195: "+base58);
        //-------------------------------------------------------------------------------------------------------------------------





        //-------------------------------------------------------------------------------------------------------------------------
        DeterministicKey tronKey100 = HDKeyDerivation.deriveChildKey(masterKey, new ChildNumber(100, true));

        KeyPair keyPair100 = new KeyPair(tronKey100.getPrivateKeyAsHex());
        String base58100 = KeyPair.publicKeyToBase58CheckAddress(keyPair100.getRawPair().getPublicKey());
        System.out.println("child-100: "+base58100);
        //-------------------------------------------------------------------------------------------------------------------------






        //-------------------------------------------------------------------------------------------------------------------------
        DeterministicKey tronKey1 = HDKeyDerivation.deriveChildKey(masterKey, new ChildNumber(1, true));

        KeyPair keyPair1 = new KeyPair(tronKey1.getPrivateKeyAsHex());
        String base581 = KeyPair.publicKeyToBase58CheckAddress(keyPair1.getRawPair().getPublicKey());
        System.out.println("child-1: "+base581);
        //-------------------------------------------------------------------------------------------------------------------------


    }


}
