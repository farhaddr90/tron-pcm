package com.pcm.service;

import org.bitcoinj.crypto.ChildNumber;
import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.crypto.HDKeyDerivation;
import org.bitcoinj.wallet.DeterministicSeed;

import java.util.List;

public class TronKeyGenerator extends KeyGenerator {

    private final DeterministicSeed seed;

    public TronKeyGenerator(List<String> mnemonicWords) {
        super(mnemonicWords);
        this.seed = new DeterministicSeed(mnemonicWords, null, "", DeterministicSeed.DEFAULT_SEED_ENTROPY_BITS);
    }

    public DeterministicKey masterKey() {
        return HDKeyDerivation.createMasterPrivateKey(seed.getSeedBytes());
    }

    @Override
    public DeterministicKey generate() {
        DeterministicKey parent = masterKey();
        parent = generate(parent, 44, true);
        parent = generate(parent, 195, true);
        parent = generate(parent, 0, true);
        parent = generate(parent, 0, false);
        return generate(parent, 0, false);
    }

    private DeterministicKey generate(DeterministicKey parent, int childNumber, boolean isHardened) {
        return HDKeyDerivation.deriveChildKey(parent, new ChildNumber(childNumber, isHardened));
    }

}
