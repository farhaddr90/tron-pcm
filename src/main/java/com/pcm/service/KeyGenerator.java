package com.pcm.service;

import org.bitcoinj.crypto.DeterministicKey;

import java.util.List;

public abstract class KeyGenerator {

    protected List<String> mnemonicWords;

    public KeyGenerator(List<String> mnemonicWords){
        this.mnemonicWords = mnemonicWords;
    }

    //this method is used for create master key
    public abstract DeterministicKey generate();

}
