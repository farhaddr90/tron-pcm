package com.pcm.util;

import org.bitcoinj.crypto.ChildNumber;
import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.crypto.HDPath;

public class HDWalletUtils {

    public static boolean isChildKeyOfRoot(DeterministicKey rootKey, DeterministicKey keyToCheck) {
        DeterministicKey derivedKey = deriveChildKey(rootKey, keyToCheck.getPath());
        return derivedKey != null && derivedKey.equals(keyToCheck);
    }

    private static DeterministicKey deriveChildKey(DeterministicKey rootKey, HDPath childPath) {
        DeterministicKey derivedKey = rootKey;
        for (ChildNumber childNumber : childPath) {
//            derivedKey = derivedKey.deriveSoftened(childNumber);
            derivedKey.derive(childNumber.i());
            if (derivedKey == null) {
                return null; // Return null if any derivation fails
            }
        }
        return derivedKey;
    }

//    public static DeterministicKey convertToDeterministicKey(byte[] publicKeyBytes) {
//        ECKey ecKey = ECKey.fromPublicOnly(publicKeyBytes);
//        return DeterministicKey.;
//    }
}